import { useState } from "react";
import axios from "axios";
import "./App.css";
import WhetherDetail from "./components/WhetherDetail";

function App() {
  const [city, setCity] = useState('');
  const [Weather, setWeather] = useState(null);
  const [loading,setLoading] = useState(false);

  function handleChange(e) {
    setCity(e.target.value);
  }
  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    const response = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=f0e935dc2069f36294fa405c4642bd5f`);
    setWeather(response.data);
    setLoading(false);
  }
  return (
    <>
      <div className="flex w-full items-center justify-center mt-10 flex-col">
        <div className="bg-white p-5 rounded shadow-2xl max-w-lg w-85">
          <form onSubmit={handleSubmit}>
            <input onChange={handleChange} type="text" name="cityAdd" placeholder="Search City..." className="border rounded border-teal-500 p-2"/>
            <button disabled={loading} className="flex-shrink-0 bg-teal-500 py-2 px-4 ml-2 text-white rounded">Submit</button>
          </form>
        </div>
        <WhetherDetail details={Weather} />
      </div>
    </>
  );
}

export default App;
