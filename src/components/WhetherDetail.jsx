export default function WhetherDetail({details}) {
    function toTime(timestamp) {
        const date = new Date(parseInt(timestamp+''+'000'));
        const TimeString = date.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric',hour12: true });
        return TimeString;
    }
    return(
        <div className="bg-white p-3 mt-2 rounded shadow-2xl max-w-lg w-80">
           {
            details ?
            <>
             <p><b>City:</b>{details.name}</p>
            <div className="flex mt-3 justify-between">
                <p className="border-r w-1/2"><b>Temp:</b>{(details.main.temp - 273).toFixed(2)} deg</p>
                <p><b>Weather:</b>{details.weather[0].main}</p>
            </div>
            <div className="flex mt-3 justify-between">
            <p className="border-r w-1/2"><b>Sunrise:</b>{toTime(details.sys.sunrise)}</p>
            <p><b>Sunset:</b> {toTime(details.sys.sunset)}</p>
            </div>
            </>
            :
            <p>Please Enter city for details</p>
           }
        </div> 
    )
}